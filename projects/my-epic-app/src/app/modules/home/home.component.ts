import { Component, OnInit, ViewChild} from '@angular/core';
import { Cow } from '../../core/models/cow';
import { CowService } from './../../core/services/cow.service';
import {MatSidenav} from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'my-org-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  displayedColumns: string[] = ['cowId', 'animalId','eventId', 'type', 'healthIndex','daysInLactation','lactationNumber','ageInDays','deletable','actions'];
  cowsData: Cow[] | any
  current: Cow

  constructor(private cowService: CowService, private router: Router) {}

  ngOnInit(): void {
    this.cowService.getAllCows().subscribe(res => {
      console.log(res)
      this.cowsData = res
      this.current = this.cowsData[0]
    })
  }

  updateAnimal(animalId: number) {
    console.log('click')
    this.router.navigateByUrl(`/animal/animal-edit/${animalId}`)
  }

  deleteAnimal(eventId: number): void{
    this.cowService.deleteCow(eventId).subscribe((res: any) => {
      this.cowsData = new MatTableDataSource(res);
  })
}

}
