import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnimalRoutingModule } from './animal-routing.module';
import { AnimalComponent } from './animal.component';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { CreateComponent } from './create/create.component';
import { UpdateComponent } from './update/update.component';
import { MatInputModule} from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
@NgModule({
  declarations: [AnimalComponent, CreateComponent, UpdateComponent],
  imports: [
    FormsModule,
    CommonModule,
    AnimalRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatDatepickerModule
  ],
  exports: [AnimalComponent]
})
export class AnimalModule { }
