import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cow } from '../../../core/models/cow';
import { CowService } from './../../../core/services/cow.service';

@Component({
  selector: 'my-org-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  eventId: any
  newAnimal: Cow
  oldVersion: Cow
  endDate: Date;
  reportingDateTime: Date;
  minValueDateTime: Date;
  isSubmitted: boolean = false;

  constructor(private route: ActivatedRoute, private router: Router, private cowService: CowService) { }

  ngOnInit(): void {
     this.route.params.subscribe(params => {
       this.eventId = params['id']
       this.cowService.getById(this.eventId).subscribe(res => {
         this.newAnimal = res
         this.oldVersion = res
         this.endDate = new Date(res.endDate * 1000);
         this.reportingDateTime = new Date(res.reportingDateTime*1000)
         this.minValueDateTime = new Date(res.minValueDateTime*1000)
         console.log(this.newAnimal)

       })
    })
  }

  onSubmit() {
    this.newAnimal.endDate = new Date(this.endDate).getTime();
    this.newAnimal.reportingDateTime = new Date(this.reportingDateTime).getTime();
    this.newAnimal.minValueDateTime = new Date(this.minValueDateTime).getTime();
    this.cowService.updateCow(this.newAnimal).subscribe((res:Cow) => {
      this.router.navigateByUrl('/')
      this.isSubmitted = true;
      console.log(res)
    })
  }

  ngOnDestroy(): void {
    if(!this.isSubmitted && JSON.stringify(this.newAnimal) !== JSON.stringify(this.oldVersion)) {
    alert('Your changes is not saved. All changes will be lost ');
    }
  }
}
