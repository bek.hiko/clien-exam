import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnimalComponent } from './animal.component';
import { CreateComponent } from './create/create.component';
import { UpdateComponent } from './update/update.component';

const routes: Routes = [
{
  path: 'animal-info/:id',
  component: AnimalComponent
},
{
  path: 'animal-create',
  component: CreateComponent
},
{
  path: 'animal-edit/:id',
  component: UpdateComponent
},

];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class AnimalRoutingModule { }
