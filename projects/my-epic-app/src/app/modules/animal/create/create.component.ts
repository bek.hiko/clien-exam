import { Component, OnInit } from '@angular/core';
import { Cow } from '../../../core/models/cow';
import { CowService } from './../../../core/services/cow.service';
import { Router } from '@angular/router';

@Component({
  selector: 'my-org-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  newAnimal = new Cow()
  endDate: Date;
  reportingDateTime: Date;
  minValueDateTime: Date;
  id:number
  isSubmitted: boolean = false;
  constructor(private cowService: CowService, private router: Router) { }

  ngOnInit(): void {
    this.cowService.getAllCows().subscribe(res => {
      this.id = res.length+1
    })
  }

  onSubmit() {
    // alert('Thanks for submitting! Data: ' + JSON.stringify(this.newAnimal));
    this.newAnimal.startDateTime = new Date(new Date).getTime();
    this.newAnimal.endDate = new Date(this.endDate).getTime();
    this.newAnimal.reportingDateTime = new Date(this.reportingDateTime).getTime();
    this.newAnimal.minValueDateTime = new Date(this.minValueDateTime).getTime();
    this.newAnimal.animalId = `${this.id}`;
    this.newAnimal.eventId = this.id
    console.log(this.newAnimal)
    this.cowService.createNewCow(this.newAnimal).subscribe(res => {
      this.router.navigateByUrl('/')
      this.isSubmitted = true;
    })
  }

  ngOnDestroy(): void {
    if(!this.isSubmitted && JSON.stringify(this.newAnimal) !== JSON.stringify(new Cow())) {
    alert('Your changes is not saved. All changes will be lost ');

    }
  }
}
