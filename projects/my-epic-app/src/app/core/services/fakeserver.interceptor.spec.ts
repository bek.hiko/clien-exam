import { TestBed } from '@angular/core/testing';

import { FakeserverInterceptor } from './fakeserver.interceptor';

describe('FakeserverInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      FakeserverInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: FakeserverInterceptor = TestBed.inject(FakeserverInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
