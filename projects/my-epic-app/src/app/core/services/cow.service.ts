import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Cow, ResTemplate } from './../models/cow';

@Injectable({
  providedIn: 'root'
})
export class CowService {

  constructor(
    private router: Router,
    private http: HttpClient,) { }

  // get cows
  getAllCows() {
    return this.http.get<Cow[]>("http://localhost:4200/api/cows");
  }

  getById(cowid: number) {
    return this.http.get<Cow>(`http://localhost:4200/api/getbyId/${cowid}` )
  }

  createNewCow(cow: Cow) {
    return this.http.post("http://localhost:4200/api/create/cow", cow )
  }

  updateCow(updatedCow: Cow):Cow | any {
    return this.http.post("http://localhost:4200/api/update/cow", updatedCow )
  }

  deleteCow(eventId: number) {
    return this.http.delete(`http://localhost:4200/api/delete/${eventId}` )
  }
}
