export class ResTemplate {
  offset: number;
  limit: number;
  total: number;
  result: Cow[]
}

export class Cow {
  healthIndex: number;
  endDate: number;
  minValueDateTime: number;
  type: string;
  cowId: number;
  animalId: string;
  eventId: number;
  deletable: boolean;
  lactationNumber: number;
  daysInLactation: number;
  ageInDays: number;
  startDateTime: number;
  reportingDateTime: number;
}
